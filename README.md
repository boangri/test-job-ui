# UI web interface for the service of notification

Административная панель к сервису рассылки уведомлений.

Демо [здесь](https://tj-ui.xland.ru/)

Реализована авторизация по протоколу Oauth2 на сервисе [auth0.com](https://auth0.com)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
